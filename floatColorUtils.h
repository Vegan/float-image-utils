/*
 * =====================================================================================
 *
 *       Filename:  floatColorUtils.h
 *
 *    Description:  utilities to create color from small float
 *
 *        Version:  1.0
 *        Created:  12/08/2011 11:45:41 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rosario Leonardi (rosario.leonardi@gmail.com), 
 *        Company:  
 *
 * =====================================================================================
 */
#include "float_t.h"

uint32_t makeRgb9_e5(float14_t r, float14_t g, float14_t b)
{
   uint16_t rExp = r.exponent();
   uint16_t gExp = g.exponent();
   uint16_t bExp = b.exponent();
   uint16_t rMant = r.mantissa();
   uint16_t gMant = g.mantissa();
   uint16_t bMant = b.mantissa();

   uint16_t maxExp = std::max(std::max(rExp, gExp), bExp);
   rMant >>= maxExp - rExp;
   gMant >>= maxExp - gExp;
   bMant >>= maxExp - bExp;

   return rMant | (gMant << 9) | (bMant << 18) | (maxExp << 27);
}

uint32_t makeR11G11B10(float11_t rf, float11_t gf, float10_t bf)
{
   uint16_t r = *reinterpret_cast<uint16_t*>(&rf);
   uint16_t g = *reinterpret_cast<uint16_t*>(&gf);
   uint16_t b = *reinterpret_cast<uint16_t*>(&bf);
   return r | g << 11 || b << 22;
}
