#pragma once
#include <inttypes.h>

/*
 * =====================================================================================
 *
 *       Filename:  floatColorUtils.h
 *
 *    Description:  This class represent a generic float with one optional bit sign,
 *                  n exponent bit and m  bit for the mantissa.
 *                  The class is intent to be used only to support openGL image format like:
 *                  - rgb16f (3x float16)
 *                  - r11f_g11f_b10f (float11 float 11 float10)
 *                  - rgb9_e5 (9 bit per channel, shared 5 bit of exponent)
 *                  http://www.opengl.org/wiki/Small_Float_Formats
 *                  cause its only used for format conversion the operation + - * / are
 *                  not provided with this implementation cause they whould be really
 *                  slow and I don't want encourage to using esoteric floating point
 *                  format to perform computation.
 *                  Note: float 14, 11 and 10 don't have the sign bit. Converting from
 *                  float to a float without sign the sign will be ignored
 *        Version:  1.0
 *        Created:  12/08/2011 11:45:41 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rosario Leonardi (rosario.leonardi@gmail.com),
 *        Company:
 *
 * =====================================================================================
 */
template<typename T, bool haveSign, unsigned int bitExponent, unsigned int bitMantissa>
struct float_t
{
private:
   static const T sign_mask     = haveSign ? (1 << (bitExponent + bitMantissa)) : 0;
   static const T exp_mask      = ((1 << bitExponent) - 1) << bitMantissa;
   static const T mant_mask     = (1 << bitMantissa) - 1;
public:
   static const int num_bit_exponent = bitExponent;
   static const int num_bit_mantissa = bitMantissa;

   typedef T Representation;

   explicit float_t(float f);
   operator float() const;
   bool isNan() const
   {
      return ((_data & exp_mask) == exp_mask) && ((_data & mant_mask) != 0);
   }
   T mantissa() const
   {
      return _data & mant_mask;
   }
   T exponent() const
   {
      return (_data & exp_mask) >> num_bit_exponent;
   }
   void setBit(Representation data)
   {
      _data = data;
   }
   Representation getBit() const
   {
      return _data;
   }
private:
   // internal constant
   static const int max_exponent_value = (1 << (num_bit_exponent - 1)) - 1;
   static const int min_exponent_value = 1 - max_exponent_value;
   static const int exponent_bias = max_exponent_value;

   static const T NAN     = exp_mask | 1 << num_bit_mantissa;

   Representation _data;
};

// return A if test MSB is 1, B otherwise
inline uint32_t select(uint32_t test, uint32_t a, uint32_t b)
{
   uint32_t mask = ((int32_t)test) >> 31; // sign extension of test
   return (a & mask) | (b & ~mask);
}

inline int32_t select(uint32_t test, int32_t a, int32_t b)
{
   uint32_t mask = ((int32_t)test) >> 31; // sign extension of test
   return (a & mask) | (b & ~mask);
}

inline uint16_t select(uint32_t test, uint16_t a, uint16_t b)
{
   uint16_t mask = ((int32_t)test) >> 31; // sign extension of test
   return (a & mask) | (b & ~mask);
}

template<typename T, bool haveSign, unsigned int bitExponent, unsigned int bitMantissa>
float_t<T, haveSign, bitExponent,  bitMantissa>::float_t(float t)
{
   static const int num_bit_mantissa_float32 = 23;
   static const int num_bit_exponent_float32 = 8;
   static const int mantissa_diff = num_bit_mantissa_float32 - num_bit_mantissa;
   static const int exponent_diff = num_bit_exponent_float32 - num_bit_exponent;
   static const int float32_exponent_bias = 127;
   static const uint32_t round_mask = 1u << (mantissa_diff - 1);
   const uint32_t src = *reinterpret_cast<uint32_t *>(&t);
   const uint32_t srcExp  = 0x7F800000u & src;
   const uint32_t srcMant = 0x007FFFFFu & src;
   const uint32_t srcMantRounded = srcMant + round_mask;
   const uint32_t srcMantHiddenBit = srcMantRounded | 0x00800000u; // used for denormalized numbers

   // round can cause overflow of the mantissa, in that case we must increment the exponent
   const uint32_t isRoundOverflow = (srcMantRounded & 0x00800000u) << 8u;

   uint32_t bit_shift = mantissa_diff;
   int32_t exponent = (srcExp >> num_bit_mantissa_float32) - float32_exponent_bias;
   exponent = select(isRoundOverflow, exponent + 1, exponent);

   const uint32_t isOverflow  = max_exponent_value - exponent; // negative for overflow number
   const uint32_t isUnderflow = exponent - min_exponent_value; // negative for underflow and denormalized number
   const uint32_t bit_shift_underflow = bit_shift + 1 - (exponent_bias + exponent);
   const uint32_t Mant   = select(isUnderflow, srcMantHiddenBit, srcMant); // add mantissa hidden bit for underflow case
   const uint32_t isNan = (0x7F800000 & ~ srcExp) - 1;

   // Mantissa
   register T mantissaNan = Mant >> bit_shift;
   bit_shift = select(isUnderflow, bit_shift_underflow, bit_shift);
   register T mantissa = Mant >> bit_shift;
   mantissa = select(isOverflow, T(0x00u), mantissa);
   mantissa = select(isNan, mantissaNan, mantissa);
   // Exponent
   register T exponentT((exponent + exponent_bias) << num_bit_mantissa);
   exponentT = select(isUnderflow, T(0x00u), exponentT);
   exponentT = select(isOverflow, exp_mask, exponentT); // overflow case
   // Sign
   const T sign = ((0x80000000u & src) >> (mantissa_diff + exponent_diff)) & sign_mask;

   _data = sign | exponentT | mantissa;
}

template<typename T, bool haveSign, unsigned int bitExponent, unsigned int bitMantissa>
float_t<T, haveSign, bitExponent,  bitMantissa>::operator float() const
{
   static const int num_bit_mantissa_float32 = 23;
   static const int num_bit_exponent_float32 = 8;

   static const int mantissa_diff = num_bit_mantissa_float32 - num_bit_mantissa;
   static const int exponent_diff = num_bit_exponent_float32 - num_bit_exponent;
   const uint32_t sign = (_data & sign_mask) << (mantissa_diff + exponent_diff);
   uint32_t exp        = _data & exp_mask;
   uint32_t mantissa   = _data & mant_mask;
   switch (exp)
   {
   case exp_mask: // inf or NAN
      exp = 0x80;
      break;
   case 0: // denormalized
   {
      exp = -14;
      uint32_t mask(0x200);
      for (int i = 0; i < num_bit_mantissa; ++i)
      {
         mantissa <<= 1;
         exp--;
         if (mantissa & mask)
         {
            break;
         }
      }
      mantissa &= 0x02FF;// remove hidden bit
   }
   break;
   default: // standard case
      exp >>= num_bit_mantissa;
      exp -= exponent_bias;
   }
   exp += 127; // float exp bias
   exp <<= num_bit_mantissa_float32;
   mantissa <<= mantissa_diff;
   uint32_t t = sign | exp | mantissa;
   return *reinterpret_cast<float *>(&t);
}

typedef float_t<uint16_t, true, 5, 10> float16_t;
typedef float_t<uint16_t, false, 5, 9> float14_t;
typedef float_t<uint16_t, false, 5, 6> float11_t;
typedef float_t<uint16_t, false, 5, 5> float10_t;
