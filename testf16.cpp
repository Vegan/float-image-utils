// Test bech for float16
// Author:  Rosario Leonardi (rosario.leonardi@gmail.com),

#include "float_t.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestFixture.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/HelperMacros.h>
#include <string>
#include <iomanip>
#include <sstream>

const uint32_t testInput[] =
{
   0x00000000, //  ZERO
   0x80000000, // -ZERO
   0x38800000, // minimum normalized 2^-14
   0x38400000, // denormalized 1.5  * 2^-15
   0x38200000, // denormalized 1.25 * 2^-15
   0x37C00000, // denormalized 1.5  * 2^-16
   0x33800000, // smallest number 2^-24
   0x477FFFFF, // largest number 6440.4
   0x40000000, // 2.0
   0x42280000, // 42.0
   0x7F800000, // inf
   0xFF800000, // -inf
   0x7F810000  // NaN
};

const float16_t::Representation testResult[] =
{
   0x0000,
   0x8000,
   0x0400,
   0x0300,
   0x0280,
   0x0180,
   0x0001,
   0x7C00,
   0x4000,
   0x5140,
   0x7C00,
   0xFC00,
   0x7C08
};


template<typename T, size_t size>
size_t array_len(T (&i_array)[size])
{
   return size;
}

void composeErrorString(std::stringstream& ss, float input, float16_t::Representation expected, float16_t::Representation result)
{
   ss << " with input 0x" << std::hex  << std::setfill('0') << std::setw(8) << input << ". ";
   ss << "Expected output was 0x" << std::setw(4) << expected << " got 0x" << std::setw(4) << result;
}

using namespace CppUnit;
class TestFloat16 : public TestFixture
{
   CPPUNIT_TEST_SUITE( TestFloat16 );
   CPPUNIT_TEST( testSimpleCases );
   CPPUNIT_TEST( testNormalizedNumbers );
   //CPPUNIT_TEST( textDenormalizedNumbers );
   CPPUNIT_TEST_SUITE_END();

public:
   void setUp()
   {

   }

   void tearDown()
   {

   }

private:
   void testSimpleCases()
   {
      const int numTest = array_len(testInput);
      for (int i = 0; i < numTest; ++i)
      {
         float ieee754 = *reinterpret_cast<const float *>(&testInput[i]);
         float16_t testFloat(ieee754);
         std::stringstream ss;
         ss << "testFloat.getBit() != testResult[i]";
         composeErrorString(ss, testInput[i], testResult[i], testFloat.getBit());
         CPPUNIT_ASSERT_MESSAGE(ss.str().c_str(), testFloat.getBit() == testResult[i]);
      }
   }

   void testNormalizedNumbers()
   {
      size_t numExponents = (1 << float16_t::num_bit_exponent)-1;
      int32_t iee754FloatMantissaOffset = (23 - float16_t::num_bit_mantissa);
      size_t numDecimals = 1 << float16_t::num_bit_mantissa;
      for (size_t e = 1; e < numExponents; ++e)
      {
         size_t floatExponent = ((e - 15 + 127) << 23);
         for (size_t m = 0; m < numDecimals; ++m)
         {
            float16_t::Representation result = (e << float16_t::num_bit_mantissa) | m;
            int32_t floatRepresentation = floatExponent | (m << iee754FloatMantissaOffset);
            float ieee754 = *reinterpret_cast<const float *>(&testInput[i]);
            float16_t testFloat(ieee754);

            std::stringstream ss;
            ss << "testFloat.getBit() == result";
            composeErrorString(ss, ieee754, result, testFloat.getBit());
            
            CPPUNIT_ASSERT_MESSAGE(ss.str().c_str(), testFloat.getBit() == result);
         }
      }
   }
   #if 0
   void textDenormalizedNumbers()
   {
      size_t numDecimals = 1 << float16_t::num_bit_mantissa;
      int32_t Iee754FloatMantissaOffset = (23 - float16_t::num_bit_mantissa);
      printf("\n%d", Iee754FloatMantissaOffset);
      for (size_t m = 0; m < numDecimals; ++m)
      {
         float16_t::Representation result = m;
         int32_t floatRepresentation = (m << Iee754FloatMantissaOffset);
         float16_t testFloat(*reinterpret_cast<const float *>(&floatRepresentation));
         printf("\n%d", m);
         std::stringstream ss;
         ss << "testFloat.getBit() == result";
         composeErrorString(ss, floatRepresentation, result, testFloat.getBit());
         puts(ss.str().c_str());
         CPPUNIT_ASSERT_MESSAGE(ss.str().c_str(), testFloat.getBit() == result);       
      }
   }
   #endif
};


CPPUNIT_TEST_SUITE_REGISTRATION( TestFloat16 );

int main(int argc, char const *argv[])
{
   CPPUNIT_NS::TestResult testresult;

   // register listener for collecting the test-results
   CPPUNIT_NS::TestResultCollector collectedresults;
   testresult.addListener (&collectedresults);

   // register listener for per-test progress output
   CPPUNIT_NS::BriefTestProgressListener progress;
   testresult.addListener (&progress);

   CppUnit::TextUi::TestRunner runner;
   runner.addTest( TestFloat16::suite() );
   runner.run(testresult);

   CompilerOutputter compileroutputter(&collectedresults, std::cerr);
   compileroutputter.write ();
   /*
      uint32_t testNumber = 0x42280000;
      float testFloat =  *reinterpret_cast<float *>(&testNumber);
      uint32_t floatbit = *reinterpret_cast<uint32_t *>(&testFloat);
      float16_t a(testFloat);
      printf("0x%08X -> 0x%04X\n", floatbit, a.getBit());
      float b = (float)a;
      printf("%G -> %G\n", testFloat, b);
      if (a.isNan())
         puts("NAN\n");
      printf("--------------------------\n");
      */
   return collectedresults.wasSuccessful() ? 0 : 1;
}
